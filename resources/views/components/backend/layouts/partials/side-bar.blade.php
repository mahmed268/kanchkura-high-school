<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="{{ url('/') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <div class="sb-sidenav-menu-heading">School Website</div>
                <a class="nav-link" href="{{ route('users.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Users Table
                </a>
                <a class="nav-link" href="{{ route('topNotices.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Top Notice Table
                </a>
                <a class="nav-link" href="{{ route('sideNotices.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Side Notice Table
                </a>
                <a class="nav-link" href="{{ route('sliders.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Slider Image Table
                </a>
                <a class="nav-link" href="{{ route('messages.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Messages Table
                </a>
                <a class="nav-link" href="{{ route('instituteDetails.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Institute Details Table
                </a>
                <a class="nav-link" href="{{ route('events.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Events Table
                </a>
                <a class="nav-link" href="{{ route('galleries.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Gallery Table
                </a>
                <a class="nav-link" href="{{ route('faculties.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Faculty Table
                </a>
                <a class="nav-link" href="{{ route('staffs.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Staff Table
                </a>
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts1"
                    aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Jubilee Year
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts1" aria-labelledby="headingOne"
                    data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{ route('jubileeYearHistories.index') }}">Jubilee
                            Year
                            History
                            Table</a>
                        <a class="nav-link" href="{{ route('jubileeYearGalleries.index') }}">Jubilee
                            Year
                            Gallery
                            Table</a>
                    </nav>
                </div>

            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in email:</div>
            {{ auth()->user()->email }}
        </div>
    </nav>
</div>
