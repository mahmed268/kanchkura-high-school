<section class="header-section" style="background-image: url({{ asset('') }}ui/frontend/images/header.jpg)">
    <header class="container header-box" style="margin-bottom: -2px !important">
        <div class="header-banner row clearfix" style="display: flex; justify-content: space-evenly">
            <div class="col-md-1"></div>
            <div class="header-logo col-md-2">
                <a href="#">
                    <img src="{{ asset('') }}ui/frontend/images/khs-logo.jpg" class="img-responsive header-logo-img"
                        alt="LOGO" />
                </a>
            </div>
            <div class="header-title col-md-5 text-center" style="margin: 10px 0">
                <h1 style="display: inline; color:#006400 !important; font-family:kalpurush; font-weight:bolder">
                    কাঁচকুড়া উচ্চ বিদ্যালয়
                </h1>
                {{-- <br>
                <h1 style="display: inline; color:blue !important; font-weight:bolder; line-height:0; font-size: 32px !important "
                    class="font-bold text-uppercase">
                    Kanchkura High School
                </h1> --}}
                <a href="#" target="-blank"></a> <br />
                <span> ডাকঘরঃ কাঁচকুড়া, থানাঃ উত্তরখান,
                    ঢাকা-১২৩০</span> <br>
                <span class="text-uppercase">P.O: Kanchkura,
                    P.S: Uttarkhan, Dhaka-1230</span>
            </div>

            <div class="header-title-side col-md-3" style="margin-top: 20px; line-height:1.5; float: right !important;">
                <span> বিদ্যালয় কোডঃ ১৩৭৮</span> <br />
                <span>থানা কোডঃ ১৩৯</span> <br />
                <span>জেলা কোডঃ ১০</span> <br />
                <span class="text-uppercase">EIIN NO:
                    108550</span> <br />
                <span class="text-uppercase">MPO CODE NO:
                    2606081301</span> <br />
                <span>ফোনঃ ৮৯৯৯৩৫৬</span> <br />
            </div>
            <div class="clearBoth"></div>
        </div>
    </header>
    <x-frontend.layouts.partials.nav />
</section>
