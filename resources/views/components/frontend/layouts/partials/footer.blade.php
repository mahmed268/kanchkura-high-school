<section style="
background: url({{ asset('') }}ui/images/footer.jpg);
">
    <footer class="cs-footer-area cs-default-overlay animatedParent animateOnce"
        style="background-color: rgba(17, 15, 74)">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="cs-footer-col">
                        <h4>IMPORTANT LINK</h4>
                        <ul class="">
                            <li>
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                <a href="http://www.educationboard.gov.bd/" target="_blank">Education Board</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                <a href="http://dhakaeducationboard.portal.gov.bd/" target="_blank">Dhaka Education
                                    Board</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                <a href="http://www.moedu.gov.bd/" target="_blank">Ministry of Education</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                <a href="http://banbeis.gov.bd/new/" target="_blank">Banbeis</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                <a href="http://www.dshe.gov.bd/" target="_blank">Directorate of Secondary &amp;
                                    Higher Education</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                <a href="http://www.nu.edu.bd/" target="_blank">National University</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="cs-footer-col">
                        <h4>CONTACT US</h4>
                        <ul class="cs-footer-contact" style="color: whitesmoke">
                            <li>
                                <span>Address :</span><br />
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                Kanchkura High School<br />Kanchkura, Uttarkhan, Dhaka North City
                                Corporation,<br />Dhaka-1230.
                            </li>
                            <li>
                                <span>Call Us :</span><br />
                                <i class="fa fa-volume-control-phone" aria-hidden="true"></i>
                                01309108550
                            </li>
                            <li>
                                <span>Email :</span><br />
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                info@kanchkurahighschool.edu.bd <br>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                kanchkurahighschool77@yahoo.com
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="cs-footer-col">
                        <h4>MAP</h4>
                        <ul class="cs-footer-contact" style="color: whitesmoke">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d3648.736487660292!2d90.45322931473079!3d23.863489290364157!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3755cf53fcf376c7%3A0x93a0714e061e5da9!2sVF74%2B95V%20Kanchkura%20High%20School%2C%20Dhaka%201230!3m2!1d23.8635172!2d90.455412!5e0!3m2!1sen!2sbd!4v1646808979584!5m2!1sen!2sbd"
                                width="600" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</section>
