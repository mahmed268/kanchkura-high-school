<nav class="navbar" data-spy="affix" data-offset-top="150">
    <div class="container" style="display: flex; justify-content: space-between; margin-top:5px">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
        </div>
        <div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i></a>
                    </li>

                    <li><a href="{{ url('/at-a-glance') }}">ABOUT KHS</a></li>
                    {{-- <li><a href="#">ADMISSION</a></li> --}}
                    <li><a href="{{ url('/events-list') }}">EVENTS</a></li>
                    <li><a href="{{ url('/gallery') }}">GALLERY</a></li>
                    <li class="dropdown header-menu-dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">ADMINISTRATION</a>
                        <ul class="dropdown-menu animated fadeInLeft" aria-hidden="true">
                            <li><a href="{{ url('/faculty') }}">Faculty Members</a></li>
                            <li><a href="{{ url('/staff') }}">Staff Members</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ url('/contact') }}">CONTACT</a></li>
                    <li class="dropdown header-menu-dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">GOLDEN JUBILEE</a>
                        <ul class="dropdown-menu animated fadeInLeft" aria-hidden="true">
                            <li><a href="{{ url('/jubilee-year-history') }}">History</a>
                            </li>
                            <li><a href="{{ url('/jubilee-year-gallery') }}">Gallery</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ url('/login') }}">ADMIN</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
