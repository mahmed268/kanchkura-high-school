<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Kanchkura High School</title>
    <link rel="icon" href="{{ asset('') }}ui/frontend/images/khs-logo.jpg" type="image/icon type">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{{ asset('') }}ui/frontend/css/bootstrap.css" />
    <meta name="keywords" content="KHS, KHS School, Kanchkura High School, KHS Education, Kanchkura, KHS Notice">
    <meta name="author" content="Masum Ahmed Murad">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="{{ asset('') }}ui/frontend/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('') }}ui/frontend/css/duplStyle.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('') }}ui/frontend/css/customized.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}ui/frontend/css/banner-styles.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}ui/frontend/css/iconochive.css" />
    <link href="https://fonts.maateen.me/kalpurush/font.css" rel="stylesheet">

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
        }

    </style>
    <script>
        (function(d, s, id) {
            var js,
                fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src =
                "http://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
            fjs.parentNode.insertBefore(js, fjs);
        })(document, "script", "facebook-jssdk");
    </script>

    <!--------------------------- Header Start ------------------------>
    <x-frontend.layouts.partials.header />

    {{ $slot }}

    <!--------------------------- Footer Start ------------------------>
    <x-frontend.layouts.partials.footer />

    <!--------------------------- Copuright Start ------------------------>
    <x-frontend.layouts.partials.copyright />

    <!------------------------------------------------------------------------------------------>
    <!-- <script src="js/jquery.2.2.3.min.js"></script> -->
    <script src="{{ asset('') }}ui/frontend/js/sdk.js" async=""></script>
    <script id="facebook-jssdk" src="{{ asset('') }}ui/frontend/js/sdk_002.js"></script>
    <script src="{{ asset('') }}ui/frontend/js/analytics.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener("DOMContentLoaded", function() {
            var v = archive_analytics.values;
            v.service = "wb";
            v.server_name = "wwwb-app225.us.archive.org";
            v.server_ms = 684;
            archive_analytics.send_pageview({});
        });
    </script>
    {{-- <script src="https://gist.github.com/lazydaisy/8849711.js"></script> --}}
    <script type="text/javascript" src="{{ asset('') }}ui/frontend/js/bundle-playback.js" charset="utf-8"></script>
    <script type="text/javascript" src="{{ asset('') }}ui/frontend/js/wombat.js" charset="utf-8"></script>
    <script src="{{ asset('') }}ui/frontend/js/jquery.js"></script>
    <script src="{{ asset('') }}ui/frontend/js/bootstrap.js"></script>
    <script>
        $(document).ready(function() {
            var clickEvent = false;
            $("#myCarousel")
                .carousel({
                    interval: 4000,
                })
                .on("click", ".list-group li", function() {
                    clickEvent = true;
                    $(".list-group li").removeClass("active");
                    $(this).addClass("active");
                })
                .on("slid.bs.carousel", function(e) {
                    if (!clickEvent) {
                        var count = $(".list-group").children().length - 1;
                        var current = $(".list-group li.active");
                        current.removeClass("active").next().addClass("active");
                        var id = parseInt(current.data("slide-to"));
                        if (count == id) {
                            $(".list-group li").first().addClass("active");
                        }
                    }
                    clickEvent = false;
                });
        });

        $(window).load(function() {
            var boxheight = $("#myCarousel .carousel-inner").innerHeight();
            var itemlength = $("#myCarousel .item").length;
            var triggerheight = Math.round(boxheight / itemlength + 1);
            $("#myCarousel .list-group-item").outerHeight(triggerheight);
        });
    </script>

    <script>
        $(document).ready(function() {
            $(".navbar a, footer a[href='#myPage']").on("click", function(event) {
                if (this.hash !== "") {
                    event.preventDefault();
                    var hash = this.hash;
                    $("html, body").animate({
                            scrollTop: $(hash).offset().top,
                        },
                        900,
                        function() {
                            window.location.hash = hash;
                        }
                    );
                }
            });
            $(window).scroll(function() {
                $(".slideanim").each(function() {
                    var pos = $(this).offset().top;

                    var winTop = $(window).scrollTop();
                    if (pos < winTop + 600) {
                        $(this).addClass("slide");
                    }
                });
            });
        });
    </script>

</body>

</html>
