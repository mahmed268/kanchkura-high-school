<x-backend.layouts.master>
    <main>
        @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        <section class="section mt-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Trashed Jubilee Year Gallery Table
                        </div>
                        <div class="card-body mt-3">
                            <div class="mt-2 mb-2">
                                <a class="btn btn-sm btn-info" href="{{ route('jubileeYearGalleries.create') }}">Add
                                    New</a>
                                <a class="btn btn-sm btn-info"
                                    href="{{ route('jubileeYearGalleries.index') }}">Jubilee Year
                                    Gallery
                                    List</a>
                                <form style="display: inline" method="GET"
                                    action="{{ route('jubileeYearGalleries.trashed') }}">
                                    <input class="form-control float-end mb-2" style="width: 200px;" name="search"
                                        placeholder="Search Here" />
                                </form>
                            </div>
                            <table class="table table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        <th>Title</th>
                                        <th>File</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $sl=0 @endphp
                                    @foreach ($jubileeYearGalleries as $jubileeYearGallery)
                                        <tr>
                                            <td>{{ ++$sl }}</td>
                                            <td>{{ $jubileeYearGallery->title }}</td>
                                            <td>{{ $jubileeYearGallery->file }}</td>
                                            <td>
                                                <a class="btn btn-warning btn-sm"
                                                    href="{{ route('jubileeYearGalleries.restore', ['jubileeYearGallery' => $jubileeYearGallery->id]) }}">Restore</a>
                                                <form style="display: inline;"
                                                    action="{{ route('jubileeYearGalleries.delete', ['jubileeYearGallery' => $jubileeYearGallery->id]) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button
                                                        onclick="return confirm('Are you want to delete parmenently?')"
                                                        class="btn btn-danger btn-sm" type="submit">Parmanent
                                                        Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $jubileeYearGalleries->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-backend.layouts.master>
