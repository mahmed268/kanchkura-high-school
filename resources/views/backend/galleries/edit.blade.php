<x-backend.layouts.master>
    <main>
        @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        <section class="section mt-5">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Update Image Title
                        </div>
                        <div class="card-body mt-3">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('galleries.update', ['gallery' => $gallery->id]) }}" method="POST">
                                @csrf
                                @method('patch')
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="title" name="title"
                                        value="{{ old('title', $gallery->title) }}" type="text" />
                                    <label for="title">Title: (Character Limit: 30)</label>
                                    @error('title')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <button class="btn btn-primary btn-block" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-backend.layouts.master>
