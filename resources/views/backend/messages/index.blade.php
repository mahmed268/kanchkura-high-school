<x-backend.layouts.master>
    <main>
        @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        <section class="section mt-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Messages Table
                        </div>
                        <div class="card-body mt-3">
                            <div class="mt-2 mb-2">
                                <a class="btn btn-sm btn-info" href="{{ route('messages.create') }}">Add New</a>
                                <a class="btn btn-sm btn-info" href="{{ route('messages.trashed') }}">Trashed
                                    List</a>
                                <form style="display: inline" method="GET" action="{{ route('messages.index') }}">
                                    <input class="form-control float-end mb-2" style="width: 200px;" name="search"
                                        placeholder="Search Here" />
                                </form>
                            </div>
                            <table class="table table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        <th>Title</th>
                                        <th>Title (Bangla)</th>
                                        <th>Name</th>
                                        <th>Name (Bangla)</th>
                                        <th>Description</th>
                                        <th>Bangla Description</th>
                                        <th>File</th>
                                        <th>Created At</th>
                                        <th>Download</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $sl=0 @endphp
                                    @foreach ($messages as $message)
                                        <tr>
                                            <td>{{ ++$sl }}</td>
                                            <td>{{ $message->title }}</td>
                                            <td>{{ $message->titleBangla }}</td>
                                            <td>{{ $message->name }}</td>
                                            <td>{{ $message->nameBangla }}</td>
                                            <td>{{ $message->description }}</td>
                                            <td>{{ $message->descriptionBangla }}</td>
                                            <td><img style="height: 80px"
                                                    src="{{ asset('') }}assets/{{ $message->file }}"
                                                    alt="{{ $message->title }}"></td>
                                            <td>{{ $message->created_at->format('d/m/y') }}</td>
                                            <td><a
                                                    href="{{ route('messages.download', ['message' => $message->file]) }}">Download</a>
                                            </td>
                                            <td>
                                                <a class="btn btn-warning btn-sm"
                                                    href="{{ route('messages.edit', ['message' => $message->id]) }}">Edit</a>
                                                <form style="display: inline;"
                                                    action="{{ route('messages.destroy', ['message' => $message->id]) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button onclick="return confirm('Are you want to delete?')"
                                                        class="btn btn-danger btn-sm" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $messages->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-backend.layouts.master>
