<x-backend.layouts.master>
    <main>
        @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        <section class="section mt-5">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Update Message
                        </div>
                        <div class="card-body mt-3">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('messages.update', ['message' => $message->id]) }}" method="POST">
                                @csrf
                                @method('patch')
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="title" name="title"
                                        value="{{ old('title', $message->title) }}" type="text" />
                                    <label for="title">Title: (Charecter Limit:20)</label>
                                    @error('title')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="titleBangla" name="titleBangla"
                                        value="{{ old('titleBangla', $message->titleBangla) }}" type="text" />
                                    <label for="titleBangla">Title(Bangla): (Charecter Limit:20)</label>
                                    @error('title')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="name" name="name"
                                        value="{{ old('name', $message->name) }}" type="text" />
                                    <label for="name">Name: (Charecter Limit:40)</label>
                                    @error('name')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="nameBangla" name="nameBangla"
                                        value="{{ old('nameBangla', $message->name) }}" type="text" />
                                    <label for="nameBangla">Name: (Charecter Limit:40)</label>
                                    @error('nameBangla')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <textarea class="form-control" id="description" name="description" type="text" rows="3"
                                        style="height: 200px"> {{ old('description', $message->description) }}</textarea>
                                    <label for="description">Description: (Charecter Limit:3000)</label>
                                    @error('description')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <textarea class="form-control" id="descriptionBangla" name="descriptionBangla" type="text" rows="3"
                                        style="height: 200px"> {{ old('descriptionBangla', $message->descriptionBangla) }}</textarea>
                                    <label for="descriptionBangla">Description: (Charecter Limit:3000)</label>
                                    @error('descriptionBangla')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <button class="btn btn-primary btn-block" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-backend.layouts.master>
