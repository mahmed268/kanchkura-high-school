<x-backend.layouts.master>
    <main>
        @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        <section class="section mt-5">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Update Institute Details
                        </div>
                        <div class="card-body mt-3">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form
                                action="{{ route('instituteDetails.update', ['instituteDetail' => $instituteDetail->id]) }}"
                                method="POST">
                                @csrf
                                @method('patch')
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="title" name="title"
                                        value="{{ old('title', $instituteDetail->title) }}" type="text" />
                                    <label for="title">Title: (Character Limit: 30)</label>
                                    @error('title')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="titleBangla" name="titleBangla"
                                        value="{{ old('titleBangla', $instituteDetail->titleBangla) }}" type="text" />
                                    <label for="titleBangla">Title: (Character Limit: 50)</label>
                                    @error('titleBangla')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <textarea class="form-control" id="description" name="description" type="text" rows="3"
                                        style="height: 200px"> {{ old('description', $instituteDetail->description) }}</textarea>
                                    <label for="description">Description: (Character Limit: 4000)</label>
                                    @error('description')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <textarea class="form-control" id="descriptionBangla" name="descriptionBangla" type="text" rows="3"
                                        style="height: 200px"> {{ old('descriptionBangla', $instituteDetail->descriptionBangla) }}</textarea>
                                    <label for="descriptionBangla">Bangla Description: (Character Limit: 4000)</label>
                                    @error('descriptionBangla')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <button class="btn btn-primary btn-block" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-backend.layouts.master>
