<x-backend.layouts.master>
    <main>
        @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        <section class="section mt-5">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Update User
                        </div>
                        <div class="card-body mt-3">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('users.update', ['user' => $user->id]) }}" method="POST">
                                @csrf
                                @method('patch')
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="name" name="name"
                                        value="{{ old('name', $user->name) }}" type="text" />
                                    <label for="name">Name:</label>
                                    @error('name')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="email" name="email"
                                        value=" {{ old('email', $user->email) }}" type="text" />
                                    <label for="email">Email:</label>
                                    @error('email')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="password" name="password"
                                        value=" {{ old('password') }}" type="password" />
                                    <label for="password">Password:</label>
                                    @error('password')
                                        <span class="small text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <input class="form-control" id="name" name="remember_token"
                                    value="{{ Str::random(10) }}" type="hidden" />

                                <button class="btn btn-primary btn-block" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-backend.layouts.master>
