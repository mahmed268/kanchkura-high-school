<x-backend.layouts.master>
    <main>
        @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        <section class="section mt-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Trashed Top Notices Table
                        </div>
                        <div class="card-body mt-3">
                            <div class="mt-2 mb-2">
                                <a class="btn btn-sm btn-info" href="{{ route('topNotices.create') }}">Add New</a>
                                <a class="btn btn-sm btn-info" href="{{ route('topNotices.index') }}">Top Notice
                                    List</a>
                                <form style="display: inline" method="GET" action="{{ route('topNotices.trashed') }}">
                                    <input class="form-control float-end mb-2" style="width: 200px;" name="search"
                                        placeholder="Search Here" />
                                </form>
                            </div>
                            <table class="table table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>Sl#</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $sl=0 @endphp
                                    @foreach ($topNotices as $topNotice)
                                        <tr>
                                            <td>{{ ++$sl }}</td>
                                            <td>{{ $topNotice->title }}</td>
                                            <td>{{ $topNotice->description }}</td>
                                            <td>{{ $topNotice->file }}</td>
                                            <td>
                                                <a class="btn btn-warning btn-sm"
                                                    href="{{ route('topNotices.restore', ['topNotice' => $topNotice->id]) }}">Restore</a>
                                                <form style="display: inline;"
                                                    action="{{ route('topNotices.delete', ['topNotice' => $topNotice->id]) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button
                                                        onclick="return confirm('Are you want to delete parmenently?')"
                                                        class="btn btn-danger btn-sm" type="submit">Parmanent
                                                        Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $topNotices->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-backend.layouts.master>
