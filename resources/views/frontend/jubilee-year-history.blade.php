<x-frontend.layouts.master>
    <main>
        @foreach ($jubileeYearHistories as $jubileeYearHistory)
            <!--------------------------- After Nave ---------------------------->
            <section class="inner-header divider parallax layer-overlay overlay-dark-5">
                <div class="container pt-70 pb-20">
                    <!-- Section Content -->
                    <div class="section-content">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="text-dark text-center text-uppercase" style="font-family:kalpurush">
                                    {{ $jubileeYearHistory->title }} </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--------------------------- Body Start ---------------------------->
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 text-justify">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 margin-bottom-5" style="display: flex;justify-content: center;">
                                    <img src="{{ asset('') }}assets/{{ $jubileeYearHistory->file }}"
                                        class="img-responsive" alt="" title="" style="padding-right:15px;" align="left">
                                </div>
                                <div class="col-sm-12">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-9"></div>
                                <div class="col-md-12">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-2" style="margin-left: 40px;">
                                        <a href="{{ url('/jubilee-year-history-bangla') }}"
                                            class="btn btn-primary margin-top5P">Read in Bangla</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-xs-12 message-ul">
                                <p class="text-dark text-justify" style="margin-top: 20px; font-family:kalpurush">
                                    {{ $jubileeYearHistory->description }}</p>
                            </div>
                        </div>


                    </div>
                </div>
            </section>
        @endforeach
    </main>
</x-frontend.layouts.master>
