<x-frontend.layouts.master>

    <!--------------------------- EVENTS ------------------------>
    @include('frontend.partials.top-notice')

    <!--------------------------- SLIDER ------------------------>
    @include('frontend.partials.slider')

    <!--------------------------- About Institute ------------------------>
    <section class="cs-features-area">
        <div class="container">

            <!--------------------------- MASSAGE ------------------------>
            @include('frontend.partials.massage')

            <div class="row">
                <!--------------------------- INSTITUTE DETAILS ------------------------>
                @include('frontend.partials.institute-details')

                <div class="col-md-1 col-sm-1"></div>

                <!--------------------------- CALENDER ------------------------>
                @include('frontend.partials.calender')
            </div>
        </div>
    </section>

    <!--------------------------- EVENTS ------------------------>
    @include('frontend.partials.events')
</x-frontend.layouts.master>
