<x-frontend.layouts.master>
    <main>
        <!--------------------------- After Nave ---------------------------->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-dark text-center text-uppercase" style="font-family:kalpurush">
                                Events </h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--------------------------- Body Start ---------------------------->
        <section>
            <div class="container">
                <div class="row" style="display: flex;
                justify-content: center; ">
                    <div class="col-sm-12 col-md-8 text-justify">
                        @foreach ($events as $event)
                            <div class="row"
                                style="background-color:#ddd4d4; color:black; border: 1px rgb(116, 85, 85); padding:15px">
                                <div class="col-sm-5"><img style="height: 100px; margin-top:25px"
                                        src="{{ asset('') }}assets/{{ $event->file }}" class="img-responsive"
                                        alt="{{ $event->title }}7" title="{{ $event->title }}"></div>
                                <div class="col-sm-7">
                                    <!-- <a href="event-details?events-id=5"> -->
                                    <h4 style="margin-top:0px;">{{ $event->title }}</h4>
                                    <span style="font-size:12px;"><i class="fa fa-calendar" aria-hidden="true"></i>
                                        {{ $event->created_at->toFormattedDateString() }}</span>
                                    <p>{{ Str::limit($event->description, 50) }}
                                    </p>...
                                    <!-- </a> -->
                                    <br><span style="float:right"><a
                                            href="{{ route('events.show', ['event' => $event->id]) }}"
                                            class="btn btn-success read-more">Read more</a></span>
                                </div>
                            </div> <br>
                        @endforeach
                    </div>
                </div>
            </div>
            {{ $events->links() }}
        </section>
    </main>
</x-frontend.layouts.master>
