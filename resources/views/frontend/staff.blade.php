<x-frontend.layouts.master>
    <main>
        <!--------------------------- After Nave ---------------------------->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-dark text-center text-uppercase" style="font-family:kalpurush">
                                Staff </h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--------------------------- Body Start ---------------------------->
        <section>
            <div class="container">
                <div class="row" style="display: flex;
                justify-content: center;">
                    <div class="col-sm-12 col-md-8">
                        @foreach ($staffs as $staff)
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-2">
                                    <img style="height: 120px;" src="{{ asset('') }}assets/{{ $staff->file }}"
                                        class="img-responsive" alt="{{ $staff->name }}">
                                </div>
                                <div class="col-sm-5">
                                    <h3 style="margin-top:0;">{{ $staff->name }}</h3>
                                    <ul>
                                        <li><strong>Mobile : </strong> <b>{{ $staff->mobile }}</b></li>
                                    </ul>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-frontend.layouts.master>
