<x-frontend.layouts.master>
    <main>
        <!--------------------------- After Nave ---------------------------->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-dark text-center text-uppercase" style="font-family:kalpurush">
                                Contact </h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--------------------------- Body Start ---------------------------->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d3648.736487660292!2d90.45322931473079!3d23.863489290364157!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3755cf53fcf376c7%3A0x93a0714e061e5da9!2sVF74%2B95V%20Kanchkura%20High%20School%2C%20Dhaka%201230!3m2!1d23.8635172!2d90.455412!5e0!3m2!1sen!2sbd!4v1646808979584!5m2!1sen!2sbd"
                            width="800" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                    <div class="col-sm-4">
                        <div class="widget">
                            <h3 class="widget-title line-bottom">Office <span class="text-theme-color-3">Phone</span>
                            </h3>
                            <ul>
                                <li><i class="fa fa-volume-control-phone" aria-hidden="true"></i> 01309108550</li>
                            </ul>
                        </div><br>

                        <div class="widget">
                            <h3 class="widget-title line-bottom">Office <span class="text-theme-color-3">Email</span>
                            </h3>
                            <ul>
                                <li><i class="fa fa-envelope-o" aria-hidden="true"></i> info@kanchkurahighschool.edu.bd
                                </li>
                                <li><i class="fa fa-envelope-o" aria-hidden="true"></i> kanchkurahighschool77@yahoo.com
                                </li>
                            </ul>
                        </div><br>

                        <div class="widget">
                            <h3 class="widget-title line-bottom">Our <span class="text-theme-color-3">Address</span>
                            </h3>
                            <i class="fa fa-map-marker" aria-hidden="true"></i> Kanchkura High School, Kanchkura,
                            Uttarkhan, Dhaka North City
                            Corporation, Dhaka-1230.
                        </div><br><br><br>


                    </div>
                </div>
            </div>
        </section>
    </main>
</x-frontend.layouts.master>
