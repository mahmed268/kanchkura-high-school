<x-frontend.layouts.master>
    <main>
        <!--------------------------- After Nave ---------------------------->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-dark text-center text-uppercase" style="font-family:kalpurush">
                                এক নজরে কাঁচকুড়া উচ্চ বিদ্যালয় </h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--------------------------- Body Start ---------------------------->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 text-justify">

                        <div class="col-sm-12 margin-bottom-5" style="display: flex;justify-content: center;">
                            <img style="height: 300px" src="{{ asset('') }}assets/slider1.jpg"
                                class="img-responsive" alt="" title="" style="padding-right:15px;" align="left">
                        </div>

                        <hr>
                        <div class="col-md-12">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-2" style="margin-left: 40px;">
                                <a href="{{ url('/at-a-glance') }}" class="btn btn-primary margin-top5P">Read
                                    in English</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p>&nbsp;</p>

                            <h2 style="font-family:kalpurush"><strong>কাঁচকুড়া উচ্চ বিদ্যালয়</strong></h2>

                            <p style="font-family:kalpurush"><b>প্রতিষ্ঠিত</b><strong> &nbsp;:</strong>১৯৭০</p>

                            <h3 style="font-family:kalpurush"><strong>অবকাঠামো</strong></h3>

                            <p style="font-family:kalpurush"><strong>একাডেমিক ভবনের সংখ্যা &nbsp;: </strong>তিনটি বড়
                                বহুতল ভবন
                            </p>



                            <p style="font-family:kalpurush">শিক্ষকের সংখ্যা &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp; ৩০</p>


                            <p style="font-family:kalpurush">কর্মীদের সংখ্যা &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp;
                                ১৫</p>

                            <h3 style="font-family:kalpurush"><strong>কোর্স অফার</strong></h3>

                            <p style="font-family:kalpurush">জাতীয় পাঠ্যক্রম এবং পাঠ্যপুস্তকের নির্দেশনা অনুসারে
                                কোর্সগুলি দেওয়া হয়।</p>

                            <h3 style="font-family:kalpurush"><strong>Number of Students</strong></h3>

                            <p style="font-family:kalpurush">ছাত্রছাত্রী সংখ্যা &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
                                &nbsp; &nbsp;&nbsp; : ৩০০০</p>


                            <h3 style="font-family:kalpurush"><strong>সমাবেশের সময়সূচী</strong></h3>

                            <table cellspacing="0" cellpadding="0" border="1">
                                <tbody>
                                    <tr>
                                        <td style="height:19px; width:298px">
                                            <p style="font-family:kalpurush"><strong>&nbsp; গ্রীষ্ম (মার্চ থেকে
                                                    অক্টোবর)</strong></p>
                                        </td>
                                        <td style="height:19px; width:355px">
                                            <p><strong>&nbsp; শীতকাল (নভেম্বর থেকে ফেব্রুয়ারি)</strong></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:19px; width:298px">
                                            <p style="font-family:kalpurush">&nbsp; ০৭ঃ৩৫ থেকে ০৭ঃ৫৫ পর্যন্ত</p>
                                        </td>
                                        <td style="height:19px; width:355px">
                                            <p style="font-family:kalpurush">&nbsp; ০৮ঃ০৫ থেকে ০৮ঃ২৫ পর্যন্ত</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>

                        <hr>
                    </div>

                </div>
            </div>
        </section>
    </main>
</x-frontend.layouts.master>
