<x-frontend.layouts.master>
    <main>
        <!--------------------------- After Nave ---------------------------->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-dark text-center text-uppercase" style="font-family:kalpurush">
                                KHS At A Glance </h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--------------------------- Body Start ---------------------------->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 text-justify">

                        <div class="col-sm-12 margin-bottom-5" style="display: flex;justify-content: center;">
                            <img style="height: 300px" src="{{ asset('') }}assets/slider1.jpg"
                                class="img-responsive" alt="" title="" style="padding-right:15px;" align="left">
                        </div>

                        <hr>
                        <div class="col-md-12">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-2" style="margin-left: 40px;">
                                <a href="{{ url('/at-a-glance-bangla') }}" class="btn btn-primary margin-top5P">Read
                                    in Bangla</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p>&nbsp;</p>

                            <h2><strong>Kanchkura High School</strong></h2>

                            <p><b>Established</b><strong> &nbsp;:</strong>1970</p>

                            <h3><strong>Infrastructure</strong></h3>

                            <p><strong>Number of Academic Building &nbsp;: </strong>Three large multistoried buildings
                            </p>



                            <p>Number of Teachers &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp; 30</p>


                            <p>Number of Staffs &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp; 15</p>

                            <h3><strong>Courses Offered</strong></h3>

                            <p>Courses are offered according to the direction of the National Curriculum and Textbook
                                Board.</p>

                            <h3><strong>Number of Students</strong></h3>

                            <p>Total Students &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
                                &nbsp; &nbsp;&nbsp; : 3000</p>


                            <h3><strong>Assembly Timetable</strong></h3>

                            <table cellspacing="0" cellpadding="0" border="1">
                                <tbody>
                                    <tr>
                                        <td style="height:19px; width:298px">
                                            <p><strong>&nbsp; Summer (March to October)</strong></p>
                                        </td>
                                        <td style="height:19px; width:355px">
                                            <p><strong>&nbsp; Winter (November to February)</strong></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:19px; width:298px">
                                            <p>&nbsp; From 07:35 to 07:55</p>
                                        </td>
                                        <td style="height:19px; width:355px">
                                            <p>&nbsp; From 08:05 to 08:25</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            {{-- <h3><strong>Class and Timetable</strong></h3> --}}

                            {{-- <table style="width:648px" cellspacing="0" cellpadding="0" border="1">
                                <tbody>
                                    <tr>
                                        <td style="height:38px; width:228px">
                                            <p><strong>Class</strong></p>
                                        </td>
                                        <td style="height:38px; width:198px">
                                            <p><strong>Summer(March to October)</strong></p>
                                        </td>
                                        <td style="height:38px; width:222px">
                                            <p><strong>Winter(November to February)</strong></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:23px; width:228px">
                                            <p>Nursery &amp; KG&nbsp;(Bangla Version)</p>
                                        </td>
                                        <td style="height:23px; width:198px">
                                            <p>&nbsp; From 11:25 to 13:25</p>
                                        </td>
                                        <td style="height:23px; width:222px">
                                            <p>&nbsp; From 11:55 to 13:55</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:19px; width:228px">
                                            <p>Nursery &amp; KG (English Version)</p>
                                        </td>
                                        <td style="height:19px; width:198px">
                                            <p>&nbsp; From08:00 to 10:55</p>
                                        </td>
                                        <td style="height:19px; width:222px">
                                            <p>&nbsp; From 08:30 to 11:25</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:24px; width:228px">
                                            <p>&nbsp; Class One and Two</p>
                                        </td>
                                        <td style="height:24px; width:198px">
                                            <p>&nbsp; From08:00 to 10:55</p>
                                        </td>
                                        <td style="height:24px; width:222px">
                                            <p>&nbsp; From 08:30 to 11:25</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:25px; width:228px">
                                            <p>&nbsp; Class Three and Four</p>
                                        </td>
                                        <td style="height:25px; width:198px">
                                            <p>&nbsp; From 08:00 to 11:35</p>
                                        </td>
                                        <td style="height:25px; width:222px">
                                            <p>&nbsp; From 08:30 to 12:05</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:24px; width:228px">
                                            <p>&nbsp; Class Five</p>
                                        </td>
                                        <td style="height:24px; width:198px">
                                            <p>&nbsp; From 08:00 to 11:45</p>
                                        </td>
                                        <td style="height:24px; width:222px">
                                            <p>&nbsp; From 08:30 to 13:15</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:25px; width:228px">
                                            <p>&nbsp; Class Six to Twelve</p>
                                        </td>
                                        <td style="height:25px; width:198px">
                                            <p>&nbsp; From 08:00 to 13:25</p>
                                        </td>
                                        <td style="height:25px; width:222px">
                                            <p>&nbsp; From 08:30 to 13:55</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table> --}}
                            <!-- <div class="col-md-offset-5 col-sm-offset-5 col-xs-offset-5"> -->
                        </div>
                        <hr>

                        <hr>
                    </div>

                </div>
            </div>
        </section>
    </main>
</x-frontend.layouts.master>
