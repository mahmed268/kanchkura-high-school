<x-frontend.layouts.master>
    <main>
        <!--------------------------- After Nave ---------------------------->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5">
            <div class="container pt-70 pb-20">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-dark text-center text-uppercase" style="font-family:kalpurush">
                                Jubilee Year Image Gallery </h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--------------------------- Body Start ---------------------------->
        <section>
            <div class="container">
                <div class="row">
                    @foreach ($jubileeYearGalleries as $jubileeYearGallery)
                        <div data-toggle="modal" data-target="#exampleModal" class="col-sm-3 album-image-box"
                            style="max-height:250px;">
                            <div class="img-thumbnail content-display-box">
                                <img class="img-gallery ih-item"
                                    style="cursor: pointer; width:100%; height: 180px !important;"
                                    src="{{ asset('') }}assets/{{ $jubileeYearGallery->file }}" alt=""
                                    title="{{ $jubileeYearGallery->title }}" />
                            </div>
                            <div>{{ $jubileeYearGallery->title }}</div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            {{ $jubileeYearGallery->title }}</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img class="img-gallery ih-item" style="width:100%; !important;"
                                            src="{{ asset('') }}assets/{{ $jubileeYearGallery->file }}" alt=""
                                            title="{{ $jubileeYearGallery->title }}" />
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            {{ $jubileeYearGalleries->links() }}
        </section>
        <br>
    </main>

</x-frontend.layouts.master>
