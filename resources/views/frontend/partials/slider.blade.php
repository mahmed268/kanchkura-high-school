<section>
    <div class="row" style="
      background-image: URL('{{ asset('') }}ui/frontend/images/slide-bg.jpg');
      background-size: cover;
      height: auto;
      padding-bottom: 30px;
    ">
        <div class="container" style="padding-top: 30px">
            <div class="row">
                <div class="col-sm-8">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->

                        <div class="carousel-inner">
                            @php $i = 1; @endphp
                            @foreach ($sliders as $slider)
                                <div class="item {{ $i == '1' ? 'active' : '' }}">
                                    @php $i++; @endphp
                                    <img src="{{ asset('') }}assets/{{ $slider->file }}"
                                        alt="{{ $slider->title }}" title="No Image" class="slide-img" />
                                    <div class="carousel-caption">
                                        <p>
                                            {{ $slider->title }}
                                        </p>
                                    </div>

                                </div>
                            @endforeach
                        </div>

                        <!-- End Carousel Inner -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class=""></li>
                            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                            <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                            <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                            <li data-target="#myCarousel" data-slide-to="4" class="active"></li>
                        </ol>

                        <!-- Controls -->
                        <div class="carousel-controls">
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="fa fa-angle-left"></span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="fa fa-angle-right"></span>
                            </a>
                        </div>
                    </div>
                    <!-- End Carousel -->
                </div>

                @include('frontend.partials.video')
            </div>
        </div>
    </div>
</section>
