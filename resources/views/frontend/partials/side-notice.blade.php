<div class="row notice-scroll">
    <div class="col-sm-12">
        <marquee class="notice-marquee" scrollamount="2" behavior="scroll" direction="up" onmouseover="this.stop();"
            onmouseout="this.start();">
            @foreach ($sideNotices as $sideNotice)
                <p>
                    <a href="{{ asset('') }}assets/{{ $sideNotice->file }}" target="_blank"><i
                            class="fa fa-bell-o" aria-hidden="true"></i>{{ $sideNotice->title }}</a>
                    |
                    <small class="notice-date">Publish Date:
                        {{ $sideNotice->created_at->toFormattedDateString() }}</small>
                </p>
            @endforeach
        </marquee>
    </div>
</div>
