<div class="row">
    <div class="section-wrap">
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="row animatedParent animateOnce">
                    @foreach ($messages->slice(0, 2) as $message)
                        <div class="col-sm-6 cs-full-wd" style="margin-top: 15px">
                            <a href="{{ route('messages.show', ['message' => $message->id]) }}" class="text-center">
                                <div class="cs-features-col animated fadeInUp slow delay-250 go">
                                    <div class="cs-features-content">
                                        <img src="{{ asset('') }}assets/{{ $message->file }}"
                                            alt="{{ $message->name }}" title="{{ $message->name }}"
                                            class="pull-left" width="80" height="94" />
                                        <span class="font-white">{{ $message->title }}<br />
                                            <strong>{{ $message->name }}</strong><br /></span>
                                        Read More
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
