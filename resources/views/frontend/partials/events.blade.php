<section style="background-color: #010b13">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="text-uppercase font-weight-600 mt-0 font-28 line-bottom">
                    <span class="h2-span text-white">events</span>
                </h2>
                <hr />
                <div class="row">
                    @foreach ($events as $event)
                        <div class="col-sm-4 col-md-4 text-justify slideanim">
                            <article class="post-news">
                                <a href="#"><img src="{{ asset('') }}assets/{{ $event->file }}"
                                        alt="{{ $event->title }}" title=""
                                        class="img-responsive event-thumb-img" /></a>
                                <div class="post-news-body">
                                    <h4>
                                        <a
                                            href="{{ route('events.show', ['event' => $event->id]) }}">{{ $event->title }}</a>
                                        <br /><small>{{ $event->created_at->toFormattedDateString() }}</small>
                                    </h4>
                                    <div class="offset-top-20"></div>
                                    <div class="offset-top-20">
                                        <a class="btn btn-primary"
                                            href="{{ route('events.show', ['event' => $event->id]) }}">Read
                                            more
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
