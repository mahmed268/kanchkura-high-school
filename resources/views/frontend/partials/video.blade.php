<div class="col-sm-4">
    <div class="row">
        <div class="col-sm-12">
            <div class="video-box">
                <div class="embed-container">
                    <iframe width="898" height="505" src="https://www.youtube.com/embed/WIrpTykWswc"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.partials.side-notice')
</div>
