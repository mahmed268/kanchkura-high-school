<div class="col-md-5 col-sm-5">
    @foreach ($instituteDetails as $instituteDetail)
        <div class="cs-about-col text-justify">
            <div class="cs-section-tiltle">
                <h2 style="color: rgb(221, 27, 1) !important; margin-bottom: -15px; font-weight: bold">
                    <a style="color: rgb(221, 27, 1) !important; margin-bottom: -15px; font-weight: bold"
                        href="{{ route('instituteDetails.show', ['instituteDetail' => $instituteDetail->id]) }}"><span>{{ $instituteDetail->title }}</span></a>
                </h2>
                <hr />
                <div class="cs-title-bdr-one"></div>
                <div class="cs-title-bdr-two mb15">
                    <a class="btn btn-primary margin-top5P"
                        href="{{ route('instituteDetails.showBangla', ['instituteDetail' => $instituteDetail->id]) }}"
                        style="float: right">Read In Bangla</a>
                </div> <br>
            </div>

            <div class="" style="margin-left: 14px; margin-top:10px">
                <div class="row text-justify">
                    <div class="" style="">
                        <div class="">
                            <p>
                                {{ Str::limit($instituteDetail->description, 900) }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
