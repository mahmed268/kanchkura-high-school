<div class="col-md-6 col-sm-6">
    <div class="cs-about-col text-justify">
        <div class="cs-section-tiltle">
            <h2 style="color: rgb(221, 27, 1); font-weight: bold !important; margin-bottom: -15px">
                <span>Calender</span>
            </h2>
            <hr />
            <div class="cs-title-bdr-one"></div>
            <div class="cs-title-bdr-two mb15"></div>
        </div>

        <div class="" style="margin-left: 14px">
            <div class="row text-justify">
                <div class="" style="">
                    <div class="">
                        <iframe
                            src="https://calendar.google.com/calendar/embed?height=400&wkst=1&bgcolor=%23F09300&ctz=Asia%2FDhaka&showTz=0&showTitle=0&showNav=1&showPrint=0&showCalendars=0&src=ZW4uYmQjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&color=%230B8043"
                            style="border-width: 0" width="700" height="400" frameborder="0" scrolling="no"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
