<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJubileeYearHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jubilee_year_histories', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('titleBangla');
            $table->text('description')->nullable();
            $table->text('descriptionBangla')->nullable();
            $table->string('file');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jubilee_year_histories');
    }
}
