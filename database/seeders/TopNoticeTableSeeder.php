<?php

namespace Database\Seeders;

use App\Models\TopNotice;
use Illuminate\Database\Seeder;

class TopNoticeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TopNotice::create([
            'title' => 'Top Notice Test 1',
            'description' => 'Top Notice Description 1',
            'file' => 'Test 5.pdf'
        ]);

        TopNotice::create([
            'title' => 'Top Notice Test 2',
            'description' => 'Top Notice Description 2',
            'file' => 'Test 6.pdf'
        ]);

        TopNotice::create([
            'title' => 'Top Notice Test 3',
            'description' => 'Top Notice Description 3',
            'file' => 'Test 7.pdf'
        ]);

        TopNotice::create([
            'title' => 'Top Notice Test 4',
            'description' => 'Top Notice Description 4',
            'file' => 'Test 8.pdf'
        ]);
    }
}
