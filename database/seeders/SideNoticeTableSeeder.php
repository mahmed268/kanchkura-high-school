<?php

namespace Database\Seeders;

use App\Models\SideNotice;
use Illuminate\Database\Seeder;

class SideNoticeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SideNotice::create([
            'title' => 'Side Notice Test 1',
            'description' => 'Side Notice Description 1',
            'file' => 'Test 1.pdf'
        ]);

        SideNotice::create([
            'title' => 'Side Notice Test 2',
            'description' => 'Side Notice Description 2',
            'file' => 'Test 2.pdf'
        ]);

        SideNotice::create([
            'title' => 'Side Notice Test 3',
            'description' => 'Side Notice Description 3',
            'file' => 'Test 3.pdf'
        ]);

        SideNotice::create([
            'title' => 'Side Notice Test 4',
            'description' => 'Side Notice Description 4',
            'file' => 'Test 4.pdf'
        ]);
    }
}
