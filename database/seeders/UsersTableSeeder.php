<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super Admin',
            'email' => 'su@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret'),
            'remember_token' => Str::random(10),
        ]);
        User::create([
            'name' => 'Habib Ullah Habib',
            'email' => 'habibullah.ullah69@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('Khs108550'),
            'remember_token' => Str::random(10),
        ]);
    }
}
