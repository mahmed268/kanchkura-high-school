<?php

namespace Database\Seeders;

use App\Models\Slider;
use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::create([
            'title' => 'Slider 1',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m',
            'file' => 'slider1.jpg'
        ]);
        Slider::create([
            'title' => 'Slider 2',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m',
            'file' => 'slider2.jpg'
        ]);
        Slider::create([
            'title' => 'Slider 3',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m',
            'file' => 'slider3.jpg'
        ]);
        Slider::create([
            'title' => 'Slider 4',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m',
            'file' => 'slider4.jpg'
        ]);
        Slider::create([
            'title' => 'Slider 5',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m',
            'file' => 'slider5.jpg'
        ]);
    }
}
