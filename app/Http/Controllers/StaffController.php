<?php

namespace App\Http\Controllers;

use App\Http\Requests\StaffRequest;
use App\Models\Staff;
use Illuminate\Database\QueryException;

class StaffController extends Controller
{
    public function index()
    {
        $staff = Staff::latest();
        if (request('search')) {
            $staff = $staff
                ->where('name', 'like', '%' . request('search') . '%');
        }
        $staffs = $staff->paginate(10);
        return view('backend.staffs.index', compact('staffs'));
    }
    public function create()
    {
        return view('backend.staffs.create');
    }

    public function store(StaffRequest $request)
    {
        try {
            $data = new Staff();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->name = $request->name;
            $data->mobile = $request->mobile;
            $data->save();

            return redirect()->route('staffs.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $staff = Staff::where('id', $id)->first();
        return view('backend.staffs.edit', compact('staff'));
    }
    public function update(StaffRequest $request, $id)
    {
        try {
            Staff::where('id', $id)->update([
                'name' => $request->name,
                'mobile' => $request->mobile,
            ]);

            return redirect()->route('staffs.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {

            $staff = Staff::where('id', $id)->first();
            $staff->delete();
            return redirect()->route('staffs.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $staffCollection = Staff::onlyTrashed()->latest();
        if (request('search')) {
            $staffCollection = $staffCollection
                ->where('name', 'like', '%' . request('search') . '%');
        }
        $staffs = $staffCollection->paginate(10);
        return view('backend.staffs.trashed', compact('staffs'));
    }
    public function restore($id)
    {
        $staff = Staff::onlyTrashed()->findOrFail($id);
        $staff->restore();
        return redirect()->route('staffs.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $staff = Staff::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $staff->file));
        $staff->forceDelete();
        return redirect()->route('staffs.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
