<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Faculty;
use App\Models\Gallery;
use App\Models\InstituteDetail;
use App\Models\JubileeYearGallery;
use App\Models\JubileeYearHistory;
use App\Models\Message;
use App\Models\SideNotice;
use App\Models\Slider;
use App\Models\Staff;
use App\Models\TopNotice;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $topNotices = TopNotice::latest()->take(5)->get();
        $sideNotices = SideNotice::latest()->take(5)->get();
        $sliders = Slider::latest()->take(5)->get();
        // $messages = Message::latest()->take(2)->get();
        $messages = Message::orderBy('title')->take(2)->get(); 
        $instituteDetails = InstituteDetail::latest()->take(1)->get();
        $events = Event::latest()->take(3)->get();
        return view('frontend.index', compact('topNotices', 'sideNotices', 'sliders', 'messages', 'instituteDetails', 'events'));
    }

    public function atAGlance()
    {
        return view('frontend.at-a-glance');
    }

    public function atAGlanceBangla()
    {
        return view('frontend.at-a-glance-bangla');
    }

    public function events()
    {
        $eventsCollection = Event::latest();
        $events = $eventsCollection->paginate(5);
        return view('frontend.events', compact('events'));
    }

    public function contact()
    {
        return view('frontend.contact');
    }

    public function gallery()
    {
        $galleriesCollection = Gallery::latest();
        $galleries = $galleriesCollection->paginate(20);
        return view('frontend.gallery', compact('galleries'));
    }

    public function faculty()
    {
        $faculties = Faculty::orderBy('name', 'ASC')->get();
        // $faculties = $facultiesCollection->paginate(20);
        return view('frontend.faculty', compact('faculties'));
    }

    public function staff()
    {
        $staffs = Staff::orderBy('name', 'ASC')->get();
        // $faculties = $facultiesCollection->paginate(20);
        return view('frontend.staff', compact('staffs'));
    }

    public function jubileeYearHistory()
    {
        $jubileeYearHistories = JubileeYearHistory::latest()->take(1)->get();
        return view('frontend.jubilee-year-history', compact('jubileeYearHistories'));
    }

    public function jubileeYearHistoryBangla()
    {
        $jubileeYearHistories = JubileeYearHistory::latest()->take(1)->get();
        return view('frontend.jubilee-year-history-bangla', compact('jubileeYearHistories'));
    }

    public function jubileeYearGallery()
    {
        $jubileeYearGalleriesCollection = JubileeYearGallery::latest();
        $jubileeYearGalleries = $jubileeYearGalleriesCollection->paginate(20);
        return view('frontend.jubilee-year-gallery', compact('jubileeYearGalleries'));
    }
}
