<?php

namespace App\Http\Controllers;

use App\Http\Requests\SideNoticeRequest;
use App\Models\SideNotice;
use Illuminate\Database\QueryException;

class SideNoticeController extends Controller
{
    public function index()
    {
        $sideNotice = SideNotice::latest();
        if (request('search')) {
            $sideNotice = $sideNotice
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $sideNotices = $sideNotice->paginate(10);
        return view('backend.sideNotices.index', compact('sideNotices'));
    }
    public function create()
    {
        return view('backend.sideNotices.create');
    }

    public function store(SideNoticeRequest $request)
    {
        try {
            $data = new SideNotice();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->description = $request->description;
            $data->save();

            return redirect()->route('sideNotices.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $sideNotice = SideNotice::where('id', $id)->first();
        return view('backend.sideNotices.edit', compact('sideNotice'));
    }
    public function update(SideNoticeRequest $request, $id)
    {
        try {
            SideNotice::where('id', $id)->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return redirect()->route('sideNotices.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {

            $sideNotice = SideNotice::where('id', $id)->first();
            $sideNotice->delete();
            return redirect()->route('sideNotices.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $sideNoticeCollection = SideNotice::onlyTrashed()->latest();
        if (request('search')) {
            $sideNoticeCollection = $sideNoticeCollection
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $sideNotices = $sideNoticeCollection->paginate(10);
        return view('backend.sideNotices.trashed', compact('sideNotices'));
    }
    public function restore($id)
    {
        $sideNotice = SideNotice::onlyTrashed()->findOrFail($id);
        $sideNotice->restore();
        return redirect()->route('sideNotices.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $sideNotice = SideNotice::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $sideNotice->file));
        $sideNotice->forceDelete();
        return redirect()->route('sideNotices.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
