<?php

namespace App\Http\Controllers;

use App\Http\Requests\JubileeYearHistoryRequest;
use App\Models\JubileeYearHistory;
use Illuminate\Database\QueryException;

class JubileeYearHistoryController extends Controller
{
    public function index()
    {
        $jubileeYearHistory = JubileeYearHistory::latest();
        if (request('search')) {
            $jubileeYearHistory = $jubileeYearHistory
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $jubileeYearHistories = $jubileeYearHistory->paginate(10);
        return view('backend.jubileeYearHistories.index', compact('jubileeYearHistories'));
    }
    public function create()
    {
        return view('backend.jubileeYearHistories.create');
    }

    public function store(JubileeYearHistoryRequest $request)
    {
        try {
            $data = new JubileeYearHistory();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->titleBangla = $request->titleBangla;
            $data->description = $request->description;
            $data->descriptionBangla = $request->descriptionBangla;
            $data->save();

            return redirect()->route('jubileeYearHistories.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $jubileeYearHistory = JubileeYearHistory::where('id', $id)->first();
        return view('backend.jubileeYearHistories.edit', compact('jubileeYearHistory'));
    }
    public function update(JubileeYearHistoryRequest $request, $id)
    {
        try {
            JubileeYearHistory::where('id', $id)->update([
                'title' => $request->title,
                'titleBangla' => $request->titleBangla,
                'description' => $request->description,
                'descriptionBangla' => $request->descriptionBangla,
            ]);

            return redirect()->route('jubileeYearHistories.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function show(JubileeYearHistory $jubileeYearHistory)
    {
        return view('backend.jubileeYearHistories.show', compact('jubileeYearHistory'));
    }
    public function destroy($id)
    {
        try {

            $jubileeYearHistory = JubileeYearHistory::where('id', $id)->first();
            $jubileeYearHistory->delete();
            return redirect()->route('jubileeYearHistories.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $jubileeYearHistoryCollection = JubileeYearHistory::onlyTrashed()->latest();
        if (request('search')) {
            $jubileeYearHistoryCollection = $jubileeYearHistoryCollection
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $jubileeYearHistories = $jubileeYearHistoryCollection->paginate(10);
        return view('backend.jubileeYearHistories.trashed', compact('jubileeYearHistories'));
    }
    public function restore($id)
    {
        $jubileeYearHistory = JubileeYearHistory::onlyTrashed()->findOrFail($id);
        $jubileeYearHistory->restore();
        return redirect()->route('jubileeYearHistories.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $jubileeYearHistory = JubileeYearHistory::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $jubileeYearHistory->file));
        $jubileeYearHistory->forceDelete();
        return redirect()->route('jubileeYearHistories.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
