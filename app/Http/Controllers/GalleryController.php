<?php

namespace App\Http\Controllers;

use App\Http\Requests\GalleryRequest;
use App\Models\Gallery;
use Illuminate\Database\QueryException;

class GalleryController extends Controller
{
    public function index()
    {
        $gallery = Gallery::latest();
        if (request('search')) {
            $gallery = $gallery
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $galleries = $gallery->paginate(10);
        return view('backend.galleries.index', compact('galleries'));
    }
    public function create()
    {
        return view('backend.galleries.create');
    }

    public function store(GalleryRequest $request)
    {
        try {
            $data = new Gallery();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->save();

            return redirect()->route('galleries.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $gallery = Gallery::where('id', $id)->first();
        return view('backend.galleries.edit', compact('gallery'));
    }
    public function update(GalleryRequest $request, $id)
    {
        try {
            Gallery::where('id', $id)->update([
                'title' => $request->title,
            ]);

            return redirect()->route('galleries.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {

            $gallery = Gallery::where('id', $id)->first();
            $gallery->delete();
            return redirect()->route('galleries.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $galleryCollection = Gallery::onlyTrashed()->latest();
        if (request('search')) {
            $galleryCollection = $galleryCollection
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $galleries = $galleryCollection->paginate(10);
        return view('backend.galleries.trashed', compact('galleries'));
    }
    public function restore($id)
    {
        $gallery = Gallery::onlyTrashed()->findOrFail($id);
        $gallery->restore();
        return redirect()->route('galleries.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $gallery = Gallery::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $gallery->file));
        $gallery->forceDelete();
        return redirect()->route('galleries.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
