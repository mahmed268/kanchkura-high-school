<?php

namespace App\Http\Controllers;

use App\Http\Requests\InstituteDetailRequest;
use App\Models\InstituteDetail;
use Illuminate\Database\QueryException;

class InstituteDetailController extends Controller
{
    public function index()
    {
        $instituteDetail = InstituteDetail::latest();
        if (request('search')) {
            $instituteDetail = $instituteDetail
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $instituteDetails = $instituteDetail->paginate(10);
        return view('backend.instituteDetails.index', compact('instituteDetails'));
    }
    public function create()
    {
        return view('backend.instituteDetails.create');
    }

    public function store(InstituteDetailRequest $request)
    {
        try {
            $data = new InstituteDetail();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->description = $request->description;
            $data->descriptionBangla = $request->descriptionBangla;
            $data->save();

            return redirect()->route('instituteDetails.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $instituteDetail = InstituteDetail::where('id', $id)->first();
        return view('backend.instituteDetails.edit', compact('instituteDetail'));
    }
    public function update(InstituteDetailRequest $request, $id)
    {
        try {
            InstituteDetail::where('id', $id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'descriptionBangla' => $request->descriptionBangla,
            ]);

            return redirect()->route('instituteDetails.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function show(InstituteDetail $instituteDetail)
    {
        return view('backend.instituteDetails.show', compact('instituteDetail'));
    }

    public function showBangla(InstituteDetail $instituteDetail)
    {
        return view('backend.instituteDetails.show-bangla', compact('instituteDetail'));
    }

    public function destroy($id)
    {
        try {

            $instituteDetail = InstituteDetail::where('id', $id)->first();
            $instituteDetail->delete();
            return redirect()->route('instituteDetails.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $instituteDetailCollection = InstituteDetail::onlyTrashed()->latest();
        if (request('search')) {
            $instituteDetailCollection = $instituteDetailCollection
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $instituteDetails = $instituteDetailCollection->paginate(10);
        return view('backend.instituteDetails.trashed', compact('instituteDetails'));
    }
    public function restore($id)
    {
        $instituteDetail = InstituteDetail::onlyTrashed()->findOrFail($id);
        $instituteDetail->restore();
        return redirect()->route('instituteDetails.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $instituteDetail = InstituteDetail::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $instituteDetail->file));
        $instituteDetail->forceDelete();
        return redirect()->route('instituteDetails.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
