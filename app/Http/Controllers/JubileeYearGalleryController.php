<?php

namespace App\Http\Controllers;

use App\Http\Requests\JubileeYearGalleryRequest;
use App\Models\JubileeYearGallery;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class JubileeYearGalleryController extends Controller
{
    public function index()
    {
        $jubileeYearGallery = JubileeYearGallery::latest();
        if (request('search')) {
            $jubileeYearGallery = $jubileeYearGallery
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $jubileeYearGalleries = $jubileeYearGallery->paginate(10);
        return view('backend.jubileeYearGalleries.index', compact('jubileeYearGalleries'));
    }
    public function create()
    {
        return view('backend.jubileeYearGalleries.create');
    }

    public function store(JubileeYearGalleryRequest $request)
    {
        try {
            $data = new JubileeYearGallery();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->save();

            return redirect()->route('jubileeYearGalleries.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $jubileeYearGallery = JubileeYearGallery::where('id', $id)->first();
        return view('backend.jubileeYearGalleries.edit', compact('jubileeYearGallery'));
    }
    public function update(JubileeYearGalleryRequest $request, $id)
    {
        try {
            JubileeYearGallery::where('id', $id)->update([
                'title' => $request->title,
            ]);

            return redirect()->route('jubileeYearGalleries.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {

            $jubileeYearGallery = JubileeYearGallery::where('id', $id)->first();
            $jubileeYearGallery->delete();
            return redirect()->route('jubileeYearGalleries.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $jubileeYearGalleryCollection = JubileeYearGallery::onlyTrashed()->latest();
        if (request('search')) {
            $jubileeYearGalleryCollection = $jubileeYearGalleryCollection
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $jubileeYearGalleries = $jubileeYearGalleryCollection->paginate(10);
        return view('backend.jubileeYearGalleries.trashed', compact('jubileeYearGalleries'));
    }
    public function restore($id)
    {
        $jubileeYearGallery = JubileeYearGallery::onlyTrashed()->findOrFail($id);
        $jubileeYearGallery->restore();
        return redirect()->route('jubileeYearGalleries.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $jubileeYearGallery = JubileeYearGallery::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $jubileeYearGallery->file));
        $jubileeYearGallery->forceDelete();
        return redirect()->route('jubileeYearGalleries.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
