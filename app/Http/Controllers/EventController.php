<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventRequest;
use App\Models\Event;
use Illuminate\Database\QueryException;

class EventController extends Controller
{
    public function index()
    {
        $event = Event::latest();
        if (request('search')) {
            $event = $event
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $events = $event->paginate(10);
        return view('backend.events.index', compact('events'));
    }
    public function create()
    {
        return view('backend.events.create');
    }

    public function store(EventRequest $request)
    {
        try {
            $validated = $request->validated();

            $data = new Event();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->titleBangla = $request->titleBangla;
            $data->description = $request->description;
            $data->descriptionBangla = $request->descriptionBangla;
            $data->save();

            return redirect()->route('events.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $event = Event::where('id', $id)->first();
        return view('backend.events.edit', compact('event'));
    }
    public function update(EventRequest $request, $id)
    {
        try {
            Event::where('id', $id)->update([
                'title' => $request->title,
                'titleBangla' => $request->titleBangla,
                'description' => $request->description,
                'descriptionBangla' => $request->descriptionBangla,
            ]);

            return redirect()->route('events.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function show(Event $event)
    {
        return view('backend.events.show', compact('event'));
    }
    public function showBangla(Event $event)
    {
        return view('backend.events.show-bangla', compact('event'));
    }
    public function destroy($id)
    {
        try {

            $event = Event::where('id', $id)->first();
            $event->delete();
            return redirect()->route('events.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $eventCollection = Event::onlyTrashed()->latest();
        if (request('search')) {
            $eventCollection = $eventCollection
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $events = $eventCollection->paginate(10);
        return view('backend.events.trashed', compact('events'));
    }
    public function restore($id)
    {
        $event = Event::onlyTrashed()->findOrFail($id);
        $event->restore();
        return redirect()->route('events.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $event = Event::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $event->file));
        $event->forceDelete();
        return redirect()->route('events.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
