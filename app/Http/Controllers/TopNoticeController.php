<?php

namespace App\Http\Controllers;

use App\Http\Requests\TopNoticeRequest;
use App\Models\TopNotice;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TopNoticeController extends Controller
{
    public function index()
    {
        $topNotice = TopNotice::latest();
        if (request('search')) {
            $topNotice = $topNotice
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $topNotices = $topNotice->paginate(10);
        return view('backend.topNotices.index', compact('topNotices'));
    }
    public function create()
    {
        return view('backend.topNotices.create');
    }

    public function store(TopNoticeRequest $request)
    {
        try {
            $data = new TopNotice();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->description = $request->description;
            $data->save();

            return redirect()->route('topNotices.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download(Request $request, $file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $topNotice = TopNotice::where('id', $id)->first();
        return view('backend.topNotices.edit', compact('topNotice'));
    }
    public function update(TopNoticeRequest $request, $id)
    {
        try {
            TopNotice::where('id', $id)->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return redirect()->route('topNotices.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {

            $topNotice = TopNotice::where('id', $id)->first();
            $topNotice->delete();
            return redirect()->route('topNotices.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $topNoticeCollection = TopNotice::onlyTrashed()->latest();
        if (request('search')) {
            $topNoticeCollection = $topNoticeCollection
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $topNotices = $topNoticeCollection->paginate(10);
        return view('backend.topNotices.trashed', compact('topNotices'));
    }
    public function restore($id)
    {
        $topNotice = TopNotice::onlyTrashed()->findOrFail($id);
        $topNotice->restore();
        return redirect()->route('topNotices.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $topNotice = TopNotice::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $topNotice->file));
        $topNotice->forceDelete();
        return redirect()->route('topNotices.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
