<?php

namespace App\Http\Controllers;

use App\Http\Requests\FacultyRequest;
use App\Models\Faculty;
use Illuminate\Database\QueryException;

class FacultyController extends Controller
{
    public function index()
    {
        $faculty = Faculty::latest();
        if (request('search')) {
            $faculty = $faculty
                ->where('name', 'like', '%' . request('search') . '%');
        }
        $faculties = $faculty->paginate(10);
        return view('backend.faculties.index', compact('faculties'));
    }
    public function create()
    {
        return view('backend.faculties.create');
    }

    public function store(FacultyRequest $request)
    {
        try {
            $data = new Faculty();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->name = $request->name;
            $data->mobile = $request->mobile;
            $data->save();

            return redirect()->route('faculties.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $faculty = Faculty::where('id', $id)->first();
        return view('backend.faculties.edit', compact('faculty'));
    }
    public function update(FacultyRequest $request, $id)
    {
        try {
            Faculty::where('id', $id)->update([
                'name' => $request->name,
                'mobile' => $request->mobile,
            ]);

            return redirect()->route('faculties.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {

            $faculty = Faculty::where('id', $id)->first();
            $faculty->delete();
            return redirect()->route('faculties.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $facultyCollection = Faculty::onlyTrashed()->latest();
        if (request('search')) {
            $facultyCollection = $facultyCollection
                ->where('name', 'like', '%' . request('search') . '%');
        }
        $faculties = $facultyCollection->paginate(10);
        return view('backend.faculties.trashed', compact('faculties'));
    }
    public function restore($id)
    {
        $faculty = Faculty::onlyTrashed()->findOrFail($id);
        $faculty->restore();
        return redirect()->route('faculties.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $faculty = Faculty::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $faculty->file));
        $faculty->forceDelete();
        return redirect()->route('faculties.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
