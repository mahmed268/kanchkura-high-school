<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Models\Message;
use Illuminate\Database\QueryException;

class MessageController extends Controller
{
    public function index()
    {
        $message = Message::latest();
        if (request('search')) {
            $message = $message
                ->where('title', 'like', '%' . request('search') . '%')
                ->orWhere('name', 'like', '%' . request('search') . '%');
        }
        $messages = $message->paginate(10);
        return view('backend.messages.index', compact('messages'));
    }
    public function create()
    {
        return view('backend.messages.create');
    }

    public function store(MessageRequest $request)
    {
        try {
            $data = new Message();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->titleBangla = $request->titleBangla;
            $data->name = $request->name;
            $data->nameBangla = $request->nameBangla;
            $data->description = $request->description;
            $data->descriptionBangla = $request->descriptionBangla;
            $data->save();

            return redirect()->route('messages.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $message = Message::where('id', $id)->first();
        return view('backend.messages.edit', compact('message'));
    }
    public function update(MessageRequest $request, $id)
    {
        try {
            Message::where('id', $id)->update([
                'title' => $request->title,
                'titleBangla' => $request->titleBangla,
                'name' => $request->name,
                'nameBangla' => $request->nameBangla,
                'description' => $request->description,
                'descriptionBangla' => $request->descriptionBangla,
            ]);

            return redirect()->route('messages.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function show(Message $message)
    {
        return view('backend.messages.show', compact('message'));
    }

    public function showBangla(Message $message)
    {
        return view('backend.messages.show-bangla', compact('message'));
    }

    public function destroy($id)
    {
        try {

            $message = Message::where('id', $id)->first();
            $message->delete();
            return redirect()->route('messages.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $messageCollection = Message::onlyTrashed()->latest();
        if (request('search')) {
            $messageCollection = $messageCollection
                ->where('title', 'like', '%' . request('search') . '%')
                ->orWhere('name', 'like', '%' . request('search') . '%');
        }
        $messages = $messageCollection->paginate(10);
        return view('backend.messages.trashed', compact('messages'));
    }
    public function restore($id)
    {
        $message = Message::onlyTrashed()->findOrFail($id);
        $message->restore();
        return redirect()->route('messages.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $message = Message::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $message->file));
        $message->forceDelete();
        return redirect()->route('messages.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
