<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function index()
    {
        $user = User::latest();
        if (request('search')) {
            $user = $user
                ->where('name', 'like', '%' . request('search') . '%');
        }
        $users = $user->paginate(20);
        return view('backend.users.index', compact('users'));
    }
    public function create()
    {
        return view('backend.users.create');
    }
    public function store(UserRequest $request)
    {
        try {
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'email_verified_at' => now(),
                'password' => bcrypt($request->password),
                'remember_token' => Str::random(10),
            ]);
            return redirect()->route('users.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        return view('backend.users.edit', compact('user'));
    }
    public function update(UserRequest $request, $id)
    {
        try {
            User::where('id', $id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'email_verified_at' => now(),
                'password' => bcrypt($request->password),
                'remember_token' => Str::random(10),

            ]);

            return redirect()->route('users.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {
            $user = User::where('id', $id)->first();
            $user->delete();
            return redirect()->route('users.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $userCollection = User::onlyTrashed()->latest();
        if (request('search')) {
            $userCollection = $userCollection
                ->where('name', 'like', '%' . request('search') . '%');
        }
        $users = $userCollection->paginate(20);
        return view('backend.users.trashed', compact('users'));
    }
    public function restore($id)
    {
        $user = User::onlyTrashed()->findOrFail($id);
        $user->restore();
        return redirect()->route('users.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $user = User::onlyTrashed()->findOrFail($id);
        $user->forceDelete();
        return redirect()->route('users.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
