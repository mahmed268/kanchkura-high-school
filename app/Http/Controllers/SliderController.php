<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderRequest;
use App\Models\Slider;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public function index()
    {
        $slider = Slider::latest();
        if (request('search')) {
            $slider = $slider
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $sliders = $slider->paginate(10);

        return view('backend.sliders.index', compact('sliders'));
    }
    public function create()
    {
        return view('backend.sliders.create');
    }

    public function store(SliderRequest $request)
    {
        try {
            $data = new Slider();
            $file = $request->file;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assets', $filename);
            $data->file = $filename;
            $data->title = $request->title;
            $data->description = $request->description;
            $data->save();

            return redirect()->route('sliders.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function download($file)
    {
        return response()->download(public_path('assets/' . $file));
    }
    public function edit($id)
    {
        $slider = Slider::where('id', $id)->first();
        return view('backend.sliders.edit', compact('slider'));
    }
    public function update(SliderRequest $request, $id)
    {
        try {
            Slider::where('id', $id)->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return redirect()->route('sliders.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {

            $slider = Slider::where('id', $id)->first();
            $slider->delete();
            return redirect()->route('sliders.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function trash()
    {
        $sliderCollection = Slider::onlyTrashed()->latest();
        if (request('search')) {
            $sliderCollection = $sliderCollection
                ->where('title', 'like', '%' . request('search') . '%');
        }
        $sliders = $sliderCollection->paginate(10);
        return view('backend.sliders.trashed', compact('sliders'));
    }
    public function restore($id)
    {
        $slider = Slider::onlyTrashed()->findOrFail($id);
        $slider->restore();
        return redirect()->route('sliders.trashed')->withMessage('Successfully Restored!');
    }
    public function delete($id)
    {
        $slider = Slider::onlyTrashed()->findOrFail($id);
        unlink(public_path('assets/' . $slider->file));
        $slider->forceDelete();
        return redirect()->route('sliders.trashed')->withMessage('Successfully Deleted Permanently!');
    }
}
