<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('post')) {
            $fileRule = 'required|mimes:jpeg,png,jpg';
        } elseif (request()->isMethod('patch')) {
            $fileRule = 'sometimes';
        }
        return [
            'title' => 'required|max:20',
            'titleBangla' => 'required|max:20',
            'name' => 'required|max:40',
            'nameBangla' => 'required|max:40',
            'description' => 'max:3000',
            'descriptionBangla' => 'max:3000',
            'file' => $fileRule,
        ];
    }
}
