<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TopNoticeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('post')) {
            $fileRule = 'required';
        } elseif (request()->isMethod('patch')) {
            $fileRule = 'sometimes';
        }
        return [
            'title' => 'required|max:200',
            'description' => 'max:1000',
            'file' => $fileRule,
        ];
    }
}
