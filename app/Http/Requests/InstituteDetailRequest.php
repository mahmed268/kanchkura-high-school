<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstituteDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('post')) {
            $fileRule = 'required|mimes:jpeg,png,jpg';
        } elseif (request()->isMethod('patch')) {
            $fileRule = 'sometimes';
        }
        return [
            'title' => 'required|max:30',
            'titleBangla' => 'required|max:50',
            'description' => 'required|max:4000',
            'descriptionBangla' => 'max:4000',
            'file' => $fileRule,
        ];
    }
}
