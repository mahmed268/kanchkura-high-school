<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\FacultyController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\InstituteDetailController;
use App\Http\Controllers\JubileeYearGalleryController;
use App\Http\Controllers\JubileeYearHistoryController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\SideNoticeController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\TopNoticeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::get('/', [FrontendController::class, 'index']);
Route::get('/at-a-glance', [FrontendController::class, 'atAGlance']);
Route::get('/at-a-glance-bangla', [FrontendController::class, 'atAGlanceBangla']);
Route::get('/events-list', [FrontendController::class, 'events']);
Route::get('/contact', [FrontendController::class, 'contact']);
Route::get('/gallery', [FrontendController::class, 'gallery']);
Route::get('/faculty', [FrontendController::class, 'faculty']);
Route::get('/staff', [FrontendController::class, 'staff']);
Route::get('/jubilee-year-history', [FrontendController::class, 'jubileeYearHistory']);
Route::get('/jubilee-year-history-bangla', [FrontendController::class, 'jubileeYearHistoryBangla']);
Route::get('/jubilee-year-gallery', [FrontendController::class, 'jubileeYearGallery']);
Route::get('/messages/show/{message}', [MessageController::class, 'show'])->name('messages.show');
Route::get('/messages-bangla/show/{message}', [MessageController::class, 'showBangla'])->name('messages.showBangla');
Route::get('/instituteDetails/show/{instituteDetail}', [InstituteDetailController::class, 'show'])->name('instituteDetails.show');
Route::get('/instituteDetails-bangla/show/{instituteDetail}', [InstituteDetailController::class, 'showBangla'])->name('instituteDetails.showBangla');
Route::get('/events/show/{event}', [EventController::class, 'show'])->name('events.show');
Route::get('/events-bangla/show/{event}', [EventController::class, 'showBangla'])->name('events.showBangla');
// Route::get('/jubileeYearHistories/show/{jubileeYearHistory}', [JubileeYearHistoryController::class, 'show'])->name('jubileeYearHistories.show');

Route::middleware(['auth'])->group(function () {
    Route::get('/admin', [AdminController::class, 'index']);

    //user
    Route::resource('users', UserController::class);
    Route::get('/trashed-users', [UserController::class, 'trash'])->name('users.trashed');
    Route::get('/trashed-users/{user}/restore', [UserController::class, 'restore'])->name('users.restore');
    Route::delete('/trashed-users/{user}/delete', [UserController::class, 'delete'])->name('users.delete');

    //top-notice
    Route::resource('topNotices', TopNoticeController::class);
    Route::get('/topNotices/{topNotice}', [TopNoticeController::class, 'download'])->name('topNotices.download');
    Route::get('/trashed-topNotices', [TopNoticeController::class, 'trash'])->name('topNotices.trashed');
    Route::get('/trashed-topNotices/{topNotice}/restore', [TopNoticeController::class, 'restore'])->name('topNotices.restore');
    Route::delete('/trashed-topNotices/{topNotice}/delete', [TopNoticeController::class, 'delete'])->name('topNotices.delete');

    //side-notice
    Route::resource('sideNotices', SideNoticeController::class);
    Route::get('/sideNotices/{sideNotice}', [SideNoticeController::class, 'download'])->name('sideNotices.download');
    Route::get('/trashed-sideNotices', [SideNoticeController::class, 'trash'])->name('sideNotices.trashed');
    Route::get('/trashed-sideNotices/{sideNotice}/restore', [SideNoticeController::class, 'restore'])->name('sideNotices.restore');
    Route::delete('/trashed-sideNotices/{sideNotice}/delete', [SideNoticeController::class, 'delete'])->name('sideNotices.delete');

    //slider
    Route::resource('sliders', SliderController::class);
    Route::get('/sliders/{slider}', [SliderController::class, 'download'])->name('sliders.download');
    Route::get('/trashed-sliders', [SliderController::class, 'trash'])->name('sliders.trashed');
    Route::get('/trashed-sliders/{slider}/restore', [SliderController::class, 'restore'])->name('sliders.restore');
    Route::delete('/trashed-sliders/{slider}/delete', [SliderController::class, 'delete'])->name('sliders.delete');

    //messages
    Route::resource('messages', MessageController::class);
    Route::get('/messages/{message}', [MessageController::class, 'download'])->name('messages.download');
    Route::get('/trashed-messages', [MessageController::class, 'trash'])->name('messages.trashed');
    Route::get('/trashed-messages/{message}/restore', [MessageController::class, 'restore'])->name('messages.restore');
    Route::delete('/trashed-messages/{message}/delete', [MessageController::class, 'delete'])->name('messages.delete');

    //institute-details
    Route::resource('instituteDetails', InstituteDetailController::class);
    Route::get('/instituteDetails/{instituteDetail}', [InstituteDetailController::class, 'download'])->name('instituteDetails.download');
    Route::get('/trashed-instituteDetails', [InstituteDetailController::class, 'trash'])->name('instituteDetails.trashed');
    Route::get('/trashed-instituteDetails/{instituteDetail}/restore', [InstituteDetailController::class, 'restore'])->name('instituteDetails.restore');
    Route::delete('/trashed-instituteDetails/{instituteDetail}/delete', [InstituteDetailController::class, 'delete'])->name('instituteDetails.delete');

    //events
    Route::resource('events', EventController::class);
    Route::get('/events/{event}', [EventController::class, 'download'])->name('events.download');
    Route::get('/trashed-events', [EventController::class, 'trash'])->name('events.trashed');
    Route::get('/trashed-events/{event}/restore', [EventController::class, 'restore'])->name('events.restore');
    Route::delete('/trashed-events/{event}/delete', [EventController::class, 'delete'])->name('events.delete');

    //galleries
    Route::resource('galleries', GalleryController::class);
    Route::get('/galleries/{gallery}', [GalleryController::class, 'download'])->name('galleries.download');
    Route::get('/trashed-galleries', [GalleryController::class, 'trash'])->name('galleries.trashed');
    Route::get('/trashed-galleries/{gallery}/restore', [GalleryController::class, 'restore'])->name('galleries.restore');
    Route::delete('/trashed-galleries/{gallery}/delete', [GalleryController::class, 'delete'])->name('galleries.delete');

    //faculties
    Route::resource('faculties', FacultyController::class);
    Route::get('/faculties/{faculty}', [FacultyController::class, 'download'])->name('faculties.download');
    Route::get('/trashed-faculties', [FacultyController::class, 'trash'])->name('faculties.trashed');
    Route::get('/trashed-faculties/{faculty}/restore', [FacultyController::class, 'restore'])->name('faculties.restore');
    Route::delete('/trashed-faculties/{faculty}/delete', [FacultyController::class, 'delete'])->name('faculties.delete');

    //staffs
    Route::resource('staffs', StaffController::class);
    Route::get('/staffs/{staff}', [StaffController::class, 'download'])->name('staffs.download');
    Route::get('/trashed-staffs', [StaffController::class, 'trash'])->name('staffs.trashed');
    Route::get('/trashed-staffs/{staff}/restore', [StaffController::class, 'restore'])->name('staffs.restore');
    Route::delete('/trashed-staffs/{staff}/delete', [StaffController::class, 'delete'])->name('staffs.delete');

    //jubileeYearHistories
    Route::resource('jubileeYearHistories', JubileeYearHistoryController::class);
    Route::get('/jubileeYearHistories/{jubileeYearHistory}', [JubileeYearHistoryController::class, 'download'])->name('jubileeYearHistories.download');
    Route::get('/trashed-jubileeYearHistories', [JubileeYearHistoryController::class, 'trash'])->name('jubileeYearHistories.trashed');
    Route::get('/trashed-jubileeYearHistories/{jubileeYearHistory}/restore', [JubileeYearHistoryController::class, 'restore'])->name('jubileeYearHistories.restore');
    Route::delete('/trashed-jubileeYearHistories/{jubileeYearHistory}/delete', [JubileeYearHistoryController::class, 'delete'])->name('jubileeYearHistories.delete');

    //jubileeYearHistories
    Route::resource('jubileeYearGalleries', JubileeYearGalleryController::class);
    Route::get('/jubileeYearGalleries/{jubileeYearGallery}', [JubileeYearGalleryController::class, 'download'])->name('jubileeYearGalleries.download');
    Route::get('/trashed-jubileeYearGalleries', [JubileeYearHistoryControllerJubileeYearGalleryController::class, 'trash'])->name('jubileeYearGalleries.trashed');
    Route::get('/trashed-jubileeYearGalleries/{jubileeYearGallery}/restore', [JubileeYearGalleryController::class, 'restore'])->name('jubileeYearGalleries.restore');
    Route::delete('/trashed-jubileeYearGalleries/{jubileeYearGallery}/delete', [JubileeYearGalleryController::class, 'delete'])->name('jubileeYearGalleries.delete');
});
